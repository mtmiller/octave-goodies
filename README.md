Octave Goodies
==============

This is a collection of small utility functions for [GNU Octave].

## Usage

Install and use this package in Octave using the `pkg` command

    pkg install https://gitlab.com/mtmiller/octave-goodies/-/archive/master/octave-goodies-master.tar.gz
    pkg load goodies
    goodies

## About Octave

[GNU Octave] is a high-level interpreted language, primarily intended for
numerical computations. It provides capabilities for the numerical solution of
linear and nonlinear problems, and for performing other numerical experiments.
It also provides extensive graphics capabilities for data visualization and
manipulation. Octave is normally used through its interactive command line
interface, but it can also be used to write non-interactive programs. The
Octave language is quite similar to Matlab so that most programs are easily
portable.

## Get Involved

This Octave package is maintained at
[mtmiller/octave-goodies on GitLab](https://gitlab.com/mtmiller/octave-goodies).

If you want to contribute in any way and help make this package better, please
read the [contribution guidelines](CONTRIBUTING.md).

## License

This package is free software: you can redistribute it and/or modify it under
the terms of the [GNU General Public License][gpl] as published by the
[Free Software Foundation][fsf], either version 3 of the License, or (at your
option) any later version. See [COPYING](COPYING) for the full license text.

[GNU Octave]: https://www.octave.org/
[fsf]: https://www.fsf.org/
[gpl]: https://www.gnu.org/licenses/gpl-3.0.html
