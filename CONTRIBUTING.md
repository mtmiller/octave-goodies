# Contribution Guidelines

## Introduction

Thank you for considering contributing to this small Octave package. Any
contribution is extremely welcome and appreciated.

These guidelines are designed to help all contributors understand how to work
and interact within this project.

## Basics

- Issues and merge requests via GitLab are very welcome
- Issues and changes should be small and focused on a particular topic
- Contributions include testing, writing documentation, submitting issues,
  writing code, and proposing new features
- Contributors are expected to abide by the
  [code of conduct](CODE_OF_CONDUCT.md)

## Community

All project interaction takes place on the GitLab project
[mtmiller/octave-goodies](https://gitlab.com/mtmiller/octave-goodies). This is
a personal project for now, so the community is essentially the author at the
moment.

Since this package is intended to be used by the Octave community, questions
and discussions may be sent to the
[Octave maintainers mailing list](https://lists.gnu.org/mailman/listinfo/octave-maintainers).

If you are not familiar or comfortable with mailing lists, you can also
contact the maintainer directly at
[mtmiller@octave.org](mailto:mtmiller@octave.org).
