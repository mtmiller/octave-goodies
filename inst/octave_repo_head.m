## Copyright (C) 2019-2020 Mike Miller
## SPDX-License-Identifier: GPL-3.0-or-later
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn  {} {} octave_repo_head ()
## @deftypefnx {} {} octave_repo_head ("abbrev")
## @deftypefnx {} {} octave_repo_head ("full")
## @deftypefnx {} {} octave_repo_head ("long")
## @deftypefnx {} {} octave_repo_head ("short")
## Get the head revision of the Octave development branch.
##
## With the option @qcode{"abbrev"} or @qcode{"short"}, return the short form
## of the identifier. With the option @qcode{"full"} or @qcode{"long"}, return
## the full 40-digit identifier. By default, return the short form.
## @end deftypefn

function retval = octave_repo_head (opt)

  if (nargin > 1)
    print_usage ();
  endif

  if (nargin < 1)
    abbrev = true;
  elseif (! ischar (opt))
    error ("octave_repo_head: OPT must be a string");
  elseif (strcmp (opt, "abbrev") || strcmp (opt, "short"))
    abbrev = true;
  elseif (strcmp (opt, "full") || strcmp (opt, "long"))
    abbrev = false;
  else
    error ("octave_repo_head: invalid value for OPT: %s", opt);
  endif

  raw_revision = urlread ("https://hg.savannah.gnu.org/hgweb/octave/raw-rev/@");
  revision_id = regexp (raw_revision, "^# Node ID ([0-9a-f]+)", "lineanchors", "once", "tokens");

  if (! iscellstr (revision_id) || isempty (revision_id) || isempty (revision_id{1}))
    error ("octave_repo_head: error parsing response from remote repository");
  endif

  revision_id = revision_id{1};
  if (abbrev)
    revision_id = revision_id(1:12);
  endif

  if (nargout == 0)
    disp (revision_id);
  else
    retval = revision_id;
  endif

endfunction

## Test input validation
%!error octave_repo_head (1)
%!error octave_repo_head (1, 2)
%!error octave_repo_head ("ok")
