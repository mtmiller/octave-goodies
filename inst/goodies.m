## Copyright (C) 2019-2020 Mike Miller
## SPDX-License-Identifier: GPL-3.0-or-later
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {} {} goodies
## Describe the goodies package and its utilities.
## @end deftypefn

function goodies ()

  disp ("");
  disp ("Octave Goodies - A collection of small utility functions for GNU Octave");
  disp ("=======================================================================");
  disp ("");
  disp ("This package is a collection of utility functions. The following");
  disp ("functions are provided:");
  disp ("");
  disp ("  * basename         - Return the non-directory component of FILENAME");
  disp ("  * dirname          - Return the directory portion of FILENAME");
  disp ("  * goodies          - Describe the goodies package and its utilities");
  disp ("  * grep             - Return entries of STR or CELLSTR that match PATTERN");
  disp ("  * octave_repo_head - Get the head revision of the Octave development branch");
  disp ("  * showlogo         - Show the Octave logo");
  disp ("");

endfunction

## Mark file as being tested.  No real test needed for this function.
%!assert (1)
