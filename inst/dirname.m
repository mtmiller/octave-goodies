## Copyright (C) 2016, 2019-2020 Mike Miller
## SPDX-License-Identifier: GPL-3.0-or-later
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {} {} dirname (@var{filename})
## Return the directory portion of @var{filename}.
##
## This function conforms to the Unix command of the same name, and the
## result differs from @code{fileparts} in some notable ways.  For example,
## if @var{filename} has no directory component, the string @qcode{"."} is
## returned.  All trailing @code{filesep} characters are removed before and
## after removing the non-directory component.
##
## Example:
##
## @example
## @group
## dirname ("dir/file.m")
## @result{} dir
## dirname ("/path/to/file.m")
## @result{} /path/to
## dirname ("/path/to/dir/")
## @result{} /path/to
## @end group
## @end example
## @seealso{basename, fileparts}
## @end deftypefn

function s = dirname (filename)

  if (nargin != 1)
    print_usage ();
  endif

  if (! ischar (filename) || rows (filename) > 1)
    error ("dirname: FILENAME must be a single string");
  endif

  if (isequal (unique (filename), filesep))
    s = filesep;
    return;
  endif

  if (numel (filename) >= 2)
    idx = numel (filename);
    while (idx >= 2 && filename(idx) == filesep)
      idx--;
    endwhile;
    filename = filename(1:idx);
  endif

  [s, ~, ~] = fileparts (tilde_expand (filename));

  if (isempty (s))
    s = ".";
  elseif (numel (s) >= 2)
    idx = numel (s);
    while (idx >= 2 && s(idx) == filesep)
      idx--;
    endwhile
    s = s(1:idx);
  endif

endfunction

## Tests for equivalence to fileparts
%!assert (dirname ("file"), ".")
%!assert (dirname ("file.ext"), ".")
%!assert (dirname ("/file.ext"), "/")
%!assert (dirname ("dir/file.ext"), "dir")
%!assert (dirname ("./file.ext"), ".")
%!assert (dirname ("d1/d2/file.ext"), "d1/d2")
%!assert (dirname ("/d1/d2/file.ext"), "/d1/d2")
%!assert (dirname ("/.ext"), "/")
%!assert (dirname (".ext"), ".")

## Tests for POSIX compatibility where output differs from fileparts
%!assert (dirname (""), ".")
%!assert (dirname ("/"), "/")
%!assert (dirname ("//"), "/")
%!assert (dirname ("////////"), "/")
%!assert (dirname ("d1/d2/d3/"), "d1/d2")
%!assert (dirname ("/d1/d2/d3/"), "/d1/d2")
%!assert (dirname ("d1/d2////d3"), "d1/d2")
%!assert (dirname ("/d1/d2////d3"), "/d1/d2")
%!assert (dirname ("d1/d2////d3/"), "d1/d2")
%!assert (dirname ("/d1/d2////d3/"), "/d1/d2")
%!assert (dirname ("d1/d2////d3////"), "d1/d2")
%!assert (dirname ("/d1/d2////d3////"), "/d1/d2")

## Test tilde expansion
%!test
%! home = getenv ("HOME");
%! unwind_protect
%!   setenv ("HOME", "/home/username");
%!   assert (dirname ("~"), "/home")
%! unwind_protect_cleanup
%!   setenv ("HOME", home);
%! end_unwind_protect

## Test input validation
%!error dirname ()
%!error dirname (1,2)
%!error <FILENAME must be a single string> dirname (1)
%!error <FILENAME must be a single string> dirname (["a"; "b"])
