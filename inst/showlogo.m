## Copyright (C) 2018-2020 Mike Miller
## SPDX-License-Identifier: GPL-3.0-or-later
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn  {} {} showlogo ()
## @deftypefnx {} {} showlogo (@var{hax})
## @deftypefnx {} {@var{h} =} showlogo (@dots{})
## Show the Octave logo.
## @end deftypefn

function h = showlogo (hax)

  if (nargin > 1)
    print_usage ();
  endif

  if (nargin == 1 && ! isaxes (hax))
    error ("showlogo: HAX must be a valid axes object");
  endif

  if (compare_versions (OCTAVE_VERSION, "4.2", "<"))
    error ("showlogo: Octave version is %s, this function works with Octave 4.2 and later", OCTAVE_VERSION);
  endif

  icondir = fullfile (__octave_config_info__ ("datarootdir"), "icons");
  icon = fullfile (icondir, "hicolor", "512x512", "apps", "octave.png");

  [x, ~, alpha] = imread (icon);

  n = size (x, 1);
  idx = find (alpha == 0);
  x(idx) = 255;
  x(idx + n*n) = 256;
  x(idx + 2*n*n) = 256;

  if (nargin == 1)
    axes (hax);
  endif

  himage = imshow (x);

  if (nargout > 0)
    h = himage;
  endif

endfunction

%!testif HAVE_QT_OFFSCREEN; have_window_system () && strcmp ("qt", graphics_toolkit ())
%! hf = figure ("visible", "off");
%! himage = showlogo ();
%! assert (size (get (himage, "cdata")), [512, 512, 3])
