## Copyright (C) 2019-2020 Mike Miller
## SPDX-License-Identifier: GPL-3.0-or-later
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn  {} {} grep (@var{str}, @var{pattern})
## @deftypefnx {} {} grep (@var{cellstr}, @var{pattern})
## @deftypefnx {} {} grep (@dots{}, "-v")
## @deftypefnx {} {} grep (@dots{}, "--invert-match")
## Return entries of @var{str} or @var{cellstr} that match @var{pattern}.
##
## If called with the @qcode{"-v"} option or the @qcode{"--invert-match"}
## long option, non-matching strings are returned.
##
## Example:
##
## @example
## @group
## text = @{"first", "second", "last"@};
## grep (text, "st")
## @result{}
##     @{
##       [1,1] = first
##       [1,2] = last
##     @}
## grep (text, "st", "-v")
## @result{}
##     @{
##       [1,1] = second
##     @}
## @end group
## @end example
## @seealso{regexp}
## @end deftypefn

function retval = grep (str, pattern, opt)

  if (nargin < 2 || nargin > 3)
    print_usage ();
  endif

  if (! ischar (str) && ! iscellstr (str))
    error ("grep: STR must be a string or a cell array of strings")
  endif
  if (! ischar (pattern))
    error ("grep: PATTERN must be a string");
  endif

  invert = false;
  if (nargin < 3)
    invert = false;
  elseif (! ischar (opt))
    error ("grep: OPT must be a string");
  else
    len = length (opt);
    if (strcmp (opt, "-v"))
      invert = true;
    elseif (strncmp (opt, "--invert-match", max (len, 5)))
      invert = true;
    else
      error ("grep: invalid value for OPT: %s", opt);
    endif
  endif
  idx = regexp (str, pattern, "once");

  if (invert)
    condp = @isempty;
  else
    condp = @(_) ! isempty (_);
  endif

  if (iscellstr (str))
    retval = str(cellfun (condp, idx));
  else
    retval = ifelse (condp (idx), str, []);
  endif

endfunction

%!assert (grep ({"first", "second", "last"}, "st"), {"first", "last"})
%!assert (grep ({"first", "second", "last"}, "st", "-v"), {"second"})
%!assert (grep ({"first", "second", "last"}, "st", "--invert-match"), {"second"})

## Test input validation
%!error grep ()
%!error grep (1)
%!error grep ("a")
%!error grep ("a", 2)
%!error grep ("a", "b", 3)
%!error grep ("a", "b", "-n")
%!error grep ("a", "b", "--inverse")
