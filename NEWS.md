# Octave Goodies News

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

This is the initial release of the Octave Goodies package.

### Added
- New function `basename`
- New function `dirname`
- New function `goodies`
- New function `grep`
- New function `octave_repo_head`
- New function `showlogo`

[Unreleased]: https://gitlab.com/mtmiller/octave-goodies/compare/7be211f77e5a...master
