## Copyright (C) 2016, 2019-2020 Mike Miller
## SPDX-License-Identifier: GPL-3.0-or-later
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {} {} basename (@var{filename})
## Return the non-directory component of @var{filename}.
##
## This function conforms to the Unix command of the same name, and the
## result differs from @code{fileparts} in some notable ways.  For example,
## all trailing @code{filesep} characters are removed before determining the
## non-directory component.
##
## Example:
##
## @example
## @group
## basename ("/path/to/file.m")
## @result{} file.m
## basename ("/path/to/dir/")
## @result{} dir
## @end group
## @end example
## @seealso{dirname, fileparts}
## @end deftypefn

function s = basename (filename)

  if (nargin != 1)
    print_usage ();
  endif

  if (! ischar (filename) || rows (filename) > 1)
    error ("basename: FILENAME must be a single string");
  endif

  if (isequal (unique (filename), filesep))
    s = filesep;
    return;
  endif

  if (numel (filename) >= 2)
    idx = numel (filename);
    while (idx >= 2 && filename(idx) == filesep)
      idx--;
    endwhile;
    filename = filename(1:idx);
  endif

  [~, s, ext] = fileparts (tilde_expand (filename));

  ## work around 1×0 empty name component from Octave versions 3.x
  if (isempty (s))
    s = "";
  endif

  if (! isempty (ext))
    s = [s ext];
  endif

endfunction

## Tests for equivalence to fileparts
%!assert (basename ("file"), "file")
%!assert (basename ("file.ext"), "file.ext")
%!assert (basename ("/file.ext"), "file.ext")
%!assert (basename ("dir/file.ext"), "file.ext")
%!assert (basename ("./file.ext"), "file.ext")
%!assert (basename ("d1/d2/file.ext"), "file.ext")
%!assert (basename ("/d1/d2/file.ext"), "file.ext")
%!assert (basename ("/.ext"), ".ext")
%!assert (basename (".ext"), ".ext")

## Tests for POSIX compatibility where output differs from fileparts
%!assert (basename (""), "")
%!assert (basename ("/"), "/")
%!assert (basename ("//"), "/")
%!assert (basename ("////////"), "/")
%!assert (basename ("d1/d2/d3/"), "d3")
%!assert (basename ("/d1/d2/d3/"), "d3")
%!assert (basename ("d1/d2////d3"), "d3")
%!assert (basename ("/d1/d2////d3"), "d3")
%!assert (basename ("d1/d2////d3/"), "d3")
%!assert (basename ("/d1/d2////d3/"), "d3")
%!assert (basename ("d1/d2////d3////"), "d3")
%!assert (basename ("/d1/d2////d3////"), "d3")

## Test tilde expansion
%!test
%! home = getenv ("HOME");
%! unwind_protect
%!   setenv ("HOME", "/home/username");
%!   assert (basename ("~"), "username")
%! unwind_protect_cleanup
%!   setenv ("HOME", home);
%! end_unwind_protect

## Test input validation
%!error basename ()
%!error basename (1,2)
%!error <FILENAME must be a single string> basename (1)
%!error <FILENAME must be a single string> basename (["a"; "b"])
